const https = require('https');
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var completeresponse;

// Dynamics Connection vars
//set these values to retrieve the oauth token
const crmorg = 'https://ltctest.crm11.dynamics.com';
const clientid = '24c6abc9-2b88-4b7b-b97a-b46997e145d6';
const username = 'matt.smith@ltcharity.org.uk';
const userpassword = '31Levedale';
var tokenendpoint = 'https://login.microsoftonline.com/9d9ae523-7efa-46a0-bcd8-def0bf53c0bf/oauth2/token';

//set these values to query your crm data
const crmwebapihost = 'ltctest.crm11.dynamics.com';
// web path example: var crmwebapipath = '/api/data/v9.1/ltc_eventbookings'; //basic query to select contacts

//remove https from tokenendpoint url
tokenendpoint = tokenendpoint.toLowerCase().replace('https://','');
 
//get the authorization endpoint host name
var authhost = tokenendpoint.split('/')[0];
 
//get the authorization endpoint path
var authpath = '/' + tokenendpoint.split('/').slice(1).join('/');

function getToken()
{
    return new Promise((resolve, reject) => {
//build the authorization request
//if you want to learn more about how tokens work, see IETF RFC 6749 - https://tools.ietf.org/html/rfc6749
var reqstring = 'client_id='+clientid;
reqstring+='&resource='+encodeURIComponent(crmorg);
reqstring+='&username='+encodeURIComponent(username);
reqstring+='&password='+encodeURIComponent(userpassword);
reqstring+='&grant_type=password';
 
//set the token request parameters
var tokenrequestoptions = {
	host: authhost,
	path: authpath,
	method: 'POST',
	headers: {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Content-Length': Buffer.byteLength(reqstring)
	}
};
 
//make the token request
var tokenrequest = https.request(tokenrequestoptions, function(response) {
	//make an array to hold the response parts if we get multiple parts
	var responseparts = [];
	response.setEncoding('utf8');
	response.on('data', function(chunk) {
		//add each response chunk to the responseparts array for later
		responseparts.push(chunk);		
	});
	response.on('end', function(){
		//once we have all the response parts, concatenate the parts into a single string
		var completeresponse = responseparts.join('');
		//console.log('Response: ' + completeresponse);
		console.log('Token response retrieved . . . ');
		
		//parse the response JSON
		var tokenresponse = JSON.parse(completeresponse);
		
		//extract the token
		var token = tokenresponse.access_token;
        resolve(token);
        //console.log(token);
        
	});
});
tokenrequest.on('error', function(e) {
	console.error(e);
});
 
//post the token request data
tokenrequest.write(reqstring);
 
//close the token request
tokenrequest.end();
    });
}

function getData(token, crmwebapipath){
	return new Promise((resolve, reject) =>{
		var requestheaders = { 
			'Authorization': 'Bearer ' + token,
			'OData-MaxVersion': '4.0',
			'OData-Version': '4.0',
			'Accept': 'application/json',
			'Content-Type': 'application/json; charset=utf-8',
			'Prefer': 'odata.maxpagesize=500',
			'Prefer': 'odata.include-annotations=OData.Community.Display.V1.FormattedValue'
		};
		//set the crm request parameters
		var crmrequestoptions = {
			host: crmwebapihost,
			path: crmwebapipath,
			method: 'GET',
			headers: requestheaders
		};
		
		//make the web api request
		var crmrequest = https.request(crmrequestoptions, function(response) {
			//make an array to hold the response parts if we get multiple parts
			var responseparts = [];
			response.setEncoding('utf8');
			response.on('data', function(chunk) {
				//add each response chunk to the responseparts array for later
				responseparts.push(chunk);		
			});
			response.on('end', function(){
				//once we have all the response parts, concatenate the parts into a single string
				completeresponse = responseparts.join('');
				resolve(completeresponse);
				
			});
		});
		crmrequest.on('error', function(e) {
			console.error(e);
		});
		//close the web api request
		crmrequest.end();
	});

}

function putData(token, crmwebapipath, requestbody){
	return new Promise((resolve, reject) =>{
		var requestheaders = { 
			'Authorization': 'Bearer ' + token,
			'OData-MaxVersion': '4.0',
			'OData-Version': '4.0',
			'Accept': 'application/json',
			'Content-Type': 'application/json; charset=utf-8',
			'Prefer': 'odata.maxpagesize=500',
			'Prefer': 'odata.include-annotations=OData.Community.Display.V1.FormattedValue'
		};

        var req = new XMLHttpRequest();
        req.open("PATCH", crmwebapipath, true);
        req.setRequestHeader('Authorization', 'Bearer ' + token);
        req.setRequestHeader('OData-MaxVersion', '4.0');
        req.setRequestHeader('OData-Version', '4.0');
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        var body = JSON.stringify(requestbody);
        req.onreadystatechange = function() {
            if (this.readyState === 4) {
                req.onreadystatechange = null;;
                if (this.status === 204) {
                    resolve('204 - success')
                } else {
                    resolve (this.statusText);
                }
            }
        }
        req.send(body);
	});

}

function createData(token, crmwebapipath, requestbody){
	return new Promise((resolve, reject) =>{
		var requestheaders = { 
			'Authorization': 'Bearer ' + token,
			'OData-MaxVersion': '4.0',
			'OData-Version': '4.0',
			'Accept': 'application/json',
			'Content-Type': 'application/json; charset=utf-8',
			'Prefer': 'odata.maxpagesize=500',
			'Prefer': 'odata.include-annotations=OData.Community.Display.V1.FormattedValue'
		};

        var req = new XMLHttpRequest();
        req.open("POST", crmwebapipath, true);
        req.setRequestHeader('Authorization', 'Bearer ' + token);
        req.setRequestHeader('OData-MaxVersion', '4.0');
        req.setRequestHeader('OData-Version', '4.0');
        req.setRequestHeader("Accept", "application/json");
		req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
		req.setRequestHeader("prefer", "return=representation")
        var body = JSON.stringify(requestbody);
        req.onreadystatechange = function() {
			if (this.readyState === 4) {
				req.onreadystatechange = null;
				if (this.status === 204) {
					var uri = this.getResponseHeader("OData-EntityId");
					var regExp = /\(([^)]+)\)/;
					var matches = regExp.exec(uri);
					var newEntityId = matches[1];
					console.log(this.statusText);
				} else {
					console.log(this.status);
                    resolve (this.statusText);
                }
            }
        }
        req.send(body);
	});

}

module.exports.getToken = getToken;
module.exports.getData = getData;
module.exports.putData = putData;
module.exports.createData = createData;