const express = require('express')
const Joi = require('@hapi/joi');
const https = require('https');
const http = require('http');
const fs = require('fs');
const dynamicsConnector = require('./connector')
const _ = require('underscore');

var options = {

    key: fs.readFileSync("lvs.key"),
  
    cert: fs.readFileSync("ssl_certificate.crt"),
  
    ca: fs.readFileSync("IntermediateCA.crt")
  };

const app = express();


var token;

app.use(express.json());

// get the case id's relavant to specific contact
app.get('/api/cases/:id', (req, res) =>{
    var guid = req.params.id;
    var url = '/api/data/v9.1/contacts(' + guid + ')?$select=fullname&$expand=contact_as_responsible_contact($select=incidentid)';
    var result = makeCRMRequest(url);
    result.then(result => res.send(result));

});

// get the case id from the ticketnumber field in a case
app.get('/api/ticketnumber/:id', (req, res) => {
    var ticket = req.params.id;
    var url = '/api/data/v9.1/incidents?$select=ticketnumber&$filter=ticketnumber%20eq%20%27' + `${ticket}%27`;
    console.log(url);
    var result = makeCRMRequest(url);
    result.then(result => res.send(result));
});

// get the case details using case id's
app.get('/api/case/:id', (req, res) => {
    var guid = req.params.id;
    var url = '/api/data/v9.1/incidents(' + guid + ')';
    var result = makeCRMRequest(url);
    result.then(result => res.send(result));
});

// update the case
app.put('/api/case/:id', (req, res) => {
    var guid = req.params.id;
    var url = 'https://ltctest.crm11.dynamics.com/api/data/v9.1/incidents(' + guid + ')';
    var requestbody = req.body;
    var result =  updateCRMRequest(url, requestbody);
    result.then(result => res.send(result));
});

// update the case, by adding to fields, this is used by the volunteer report form
app.put('/api/volunteercase/:id', (req, res) => {
    var guid = req.params.id;
    var url = 'https://ltctest.crm11.dynamics.com/api/data/v9.1/incidents(' + guid + ')';
    var requestbody = req.body;
    var result = updateCRMRequestNoOverwrite(url, requestbody);
    result.then(result => res.send(result));
});

// get the contact details using contacts id's
app.get('/api/contact/:id', (req, res) => {
    var guid = req.params.id;
    var url = '/api/data/v9.1/contacts(' + guid + ')';
    var result = makeCRMRequest(url);
    result.then(result => res.send(result));
});

// update the contact
app.put('/api/contact/:id', (req, res) => {
    var guid = req.params.id;
    var url = 'https://ltctest.crm11.dynamics.com/api/data/v9.1/contacts(' + guid + ')';
    var requestbody = req.body;
    var result =  updateCRMRequest(url, requestbody);
    result.then(result => res.send(result));
});

app.put('/api/debts/', (req, res) => {
    var url = 'https://ltctest.crm11.dynamics.com/api/data/v9.1/ltc_debts';
    var requestbody = req.body;
    console.log(requestbody);
    var result = newCRMRecord(url, requestbody);
    result.then(result => res.send(result));
});

app.put('/api/notes/', (req, res) => {
    var url = 'https://ltctest.crm11.dynamics.com/api/data/v9.1/annotations';
    var requestbody = req.body;
    console.log(requestbody);
    var result = newCRMRecord(url, requestbody);
    result.then(result => res.send(result)); 

});

// PORT
const port = process.env.PORT || 3000;
//app.listen(port, () => console.log(`Listening on port ${port}...`))
https.createServer(options, app).listen(8443), () => console.log('Listening on port 443');

async function makeCRMRequest(queryURL)
{
    try {
        const token = await dynamicsConnector.getToken();
        const result = await dynamicsConnector.getData(token, queryURL);
        return result;

    }
    catch (err) {
        console.log('Error', err.message);
    }
}

async function updateCRMRequest(queryURL, requestbody)
{
    try {
        const token = await dynamicsConnector.getToken();
        const result = await dynamicsConnector.putData(token, queryURL, requestbody);
        return result;
    }
    catch (err) {
        console.log('Error', err.message);
    }
}

async function newCRMRecord(queryURL, requestbody)
{
    try {
        const token = await dynamicsConnector.getToken();
        const result = await dynamicsConnector.createData(token, queryURL, requestbody);
        return result;
    }
    catch (err) {
        console.log('Error', err.message);
    }
}

async function updateCRMRequestNoOverwrite(queryURL, requestbody)
{
    try {
        const token = await dynamicsConnector.getToken();
        const originalData = await dynamicsConnector.getData(token, queryURL);
        const jsonData = await JsonData(originalData);
        requestbody = await UpdateBodyRequest(requestbody, jsonData);
        const result = await dynamicsConnector.putData(token, queryURL, requestbody);
        return result;
    }
    catch (err) {
        console.log('Error', err.message);
    }
}

async function UpdateBodyRequest(requestbody, jsonData)
{
    try {
        for (prop in requestbody){
            for (field in jsonData){
                if (prop == field){
                    var newValue = jsonData[field] + ' ' + requestbody[prop];
                    requestbody[prop] = newValue;
                    
                }
            };
        };
        return requestbody;
    }
    catch (err) {
        console.log('Error', err.message);
    }
}

async function JsonData(requestResult)
{
    try {
        const jsonData = JSON.parse(requestResult);       
        return jsonData;
    }
    catch (err) {
        console.log('Error', err.message);
    }
}

async function getToken()
{
    try {
        const token = await dynamicsConnector.getToken();       
        return token;
    }
    catch (err) {
        console.log('Error', err.message);
    }
}