const express = require('express')
const Joi = require('@hapi/joi');
const https = require('https');
const dynamicsConnector = require('./dynamicsconnector_base.js')
const app = express();

app.use(express.json());

var token = dynamicsConnector.getToken(handleToken);
console.log(token);

app.get('/api/token', (req, res) =>
{
    res.send(token);
});

app.get('/api/eventbookings', (req, res) =>
{
    var result = displayEvents();
    result.then(result => res.send(result));
});

// PORT
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`))

function handleToken(err, result)
{
    token = result;
}

function handleData(result)
{
    var resu = result;
    console.log('called callback');
}

async function displayEvents()
{
    try {
        const result = await dynamicsConnector.getData(token)
        
        return result;
    }
    catch (err) {
        console.log('Error', err.message);
    }
}