const express = require('express')
const Joi = require('@hapi/joi');
const app = express();

app.use(express.json());

const courses = [
    {id: 1, name: 'course1'},
    {id: 2, name: 'course2'},
    {id: 3, name: 'course3'},
];

app.get('/', (req, res) =>
{
    res.send('Hello World');
});

app.get('/api/courses', (req, res) =>
{
    res.send(courses);
});

app.get('/api/courses/:id', (req, res) =>
{
    let course = courses.find(c => c.id == parseInt(req.params.id));
    if (!course) res.status(404).send('The course with the given ID was not found');
    res.send(course);

});

app.post('/api/courses', (req, res) =>
{
    const { error } = validateCourse(req.body); //result.error
    if (error) return res.status(400).send(error.details[0].message);

    const course = {
        id: courses.length + 1,
        name: req.body.name
    };
    courses.push(course);
    res.send(course);
});

app.put('/api/courses/:id', (req, res) =>
{
    // Look up the course
    // if it doesnt exist, return 404
    let course = courses.find(c => c.id == parseInt(req.params.id));
    if (!course) return res.status(404).send('The course with the given ID was not found');

    // validate
    // if invalid return 400
    const { error } = validateCourse(req.body); //result.error
    if (error) return res.status(400).send(error.details[0].message);

    // update the course
    course.name = req.body.name;
    // return the updated course to the client
    res.send(course);

});

app.delete('/api/courses/:id', (req, res) =>
{
    // Look up the course
    // not existing, return 404
    let course = courses.find(c => c.id == parseInt(req.params.id));
    if (!course) return res.status(404).send('The course with the given ID was not found');


    // delete
    const index = courses.indexOf(course);
    courses.splice(index, 1);

    // return the same course
    res.send(course);
});

//app.get('/api/posts/:year/:month', (req, res) =>
//{
//    res.send(req.params);
//});

// PORT
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`))

function validateCourse(course)
{
    const schema = Joi.object({
        name: Joi.string().min(3).required()
    });

    return schema.validate(course);
}