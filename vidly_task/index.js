const config = require('config');
const express = require('express')
const Joi = require('@hapi/joi');
const app = express();
const logger = require('./middleware/logger');
const authenticate = require('./authenticator');
const helmet = require('helmet');
const morgan = require('morgan');
const startupDebugger = require('debug')('app:startup');
// const dbDebugger = require('debug')('app:db');
const genres = require('./routes/genres')
const renderer = require('./routes/renders')

app.set('view engine', 'pug');
app.set('views', './views'); // the default setting, no need to atually set

// Configuration
startupDebugger('Application Name: ' + config.get('name'));
startupDebugger('Mail Server Address: ' + config.get('mail.host'));
startupDebugger('Mail Server Password: ' + config.get('mail.password'));

app.use(helmet());

if (app.get('env') == 'development')
{
    app.use(morgan('tiny'));
    startupDebugger('Morgan enabled');
}

app.use(express.json());
app.use(express.urlencoded( { extended: true })); // parse form responses to json objects
app.use(express.static('public')); // http://localhost:5000/readme.txt
app.use(logger);
app.use(authenticate);
app.use('/', renderer);
app.use('/api/genres', genres); // for any routes that start with this use the router loaded from the module


// PORT
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`))