// route for these endpoints is /api/genres and is defined in index.js

const express = require('express');
const router = express.Router();

const genres = [
    {id: 1, name: 'Action'},
    {id: 2, name: 'Drama'},
    {id: 3, name: 'Comedy'},
];

// Get genres list
router.get('/', (req, res) =>
{
    res.send(genres);
});

// Get specific genre by id
router.get('/:id', (req, res) =>
{
    let genre = genres.find(c => c.id == parseInt(req.params.id));
    if (!genre) res.status(404).send('The course with the given ID was not found');
    res.send(genre);
});

// Add new genre
router.post('/', (req, res) =>
{
    const { error } = validateGenre(req.body); //result.error
    if (error) return res.status(400).send(error.details[0].message);

    const genre = {
        id: genres.length + 1,
        name: req.body.name
    };
    genres.push(genre);
    res.send(genre);
});

// Update a Genre
router.put('/:id', (req, res) =>
{
    // Check genre exists
    // if not return 404
    let genre = genres.find(c => c.id == parseInt(req.params.id));
    if (!genre) return res.status(404).send('The genre with the given ID was not found');

    // Validate the update sent
    // if not valid return 400
    const { error } = validateGenre(req.body); //result.error
    if (error) return res.status(400).send(error.details[0].message);

    // Update genre
    // return genre
    genre.name = req.body.name;
    res.send(genre);

});

// Delete Genre
router.delete('/:id', (req, res) =>
{
    // Check genre exists
    // if not return 404
    let genre = genres.find(c => c.id == parseInt(req.params.id));
    if (!genre) return res.status(404).send('The genre with the given ID was not found');

        // delete
        const index = genres.indexOf(genre);
        genres.splice(index, 1);
    
        // return the same course
        res.send(genre);

});

// Validate Genre
function validateGenre(genre)
{
    const schema = Joi.object({
        name: Joi.string().min(3).required()
    });

    return schema.validate(genre);
}

module.exports = router;